﻿using System;
using System.Windows.Forms;

namespace Lab6
{
    public partial class MediaComponent : UserControl
    {
        private static float[] rowWeights = new float[3] { 0.3f, 0.3f, 0.4f };

        private bool isExpanded;
        private Guid mediaId;
        private int durationS;

        private void HideInfo()
        {
            layout.RowStyles[2].Height = 0.0f;
            expandBtn.Text = "More...";
            isExpanded = false;
        }

        private void ShowInfo()
        {
            var thisHeight = Height;
            for (int i = 0; i < Math.Min(layout.RowStyles.Count, rowWeights.Length); ++i)
            {
                layout.RowStyles[i].Height = thisHeight * rowWeights[i];
            }
            expandBtn.Text = "Less...";
            isExpanded = true;   
        }

        public MediaComponent()
        {
            InitializeComponent();
            HideInfo();

            expandBtn.Click += (object sender, EventArgs args) =>
            {
                if (isExpanded)
                {
                    HideInfo();
                }
                else
                {
                    ShowInfo();
                }
            };
        }

        public void SetMedia(Media media)
        {
            mediaId = media.Id;
            durationS = media.DurationSeconds;

            title.Text = media.Title;
            artist.Text = media.Artist;
            duration.Text = media.DurationAsString();
            additionalInfo.SetMediaInfo(media);
        }

        public Media GetMedia()
        {
            return new Media(
                mediaId,
                title.Text,
                artist.Text,
                additionalInfo.GetGenre(),
                additionalInfo.GetAlbum(),
                durationS
            );
        }

        public static MediaComponent FromMedia(Media media)
        {
            MediaComponent component = new MediaComponent();
            component.SetMedia(media);
            return component;
        }
    }
}
