﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab6
{
    public partial class AddMediaForm : Form
    {
        public AddMediaForm()
        {
            InitializeComponent();
        }

        public void RegisterHandler(EventHandler eventHandler)
        {
            saveBtn.Click += eventHandler;
        }


        public void SetMedia(Media media)
        {
            title.Text = media.Title;
            artist.Text = media.Artist;
            album.Text = media.Album;
            genre.Text = media.Genre;
            duration.Value = media.DurationSeconds;
        }

        public Media GetMedia()
        {
            return new Media(
                title.Text,
                artist.Text,
                genre.Text,
                album.Text,
                (int)duration.Value
            );
        }

    }
}
