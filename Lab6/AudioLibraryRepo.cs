﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace Lab6
{
    public class AudioLibraryRepo
    {
        private readonly static string dumpFilePath =
            Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\lab6";
        private readonly static string dumpFileName = dumpFilePath + "\\audiodb.xml";

        private readonly static DataContractSerializer mediaSerde =
            new DataContractSerializer(typeof(Media));

        private readonly IDictionary<Guid, Media> mediaLookup = new Dictionary<Guid, Media>();


        public AudioLibraryRepo()
        {
            if (!Directory.Exists(dumpFilePath))
            {
                Directory.CreateDirectory(dumpFilePath);
            }
            FileStream xmlInputStream = null;

            try
            {
                if (!File.Exists(dumpFileName))
                {
                    xmlInputStream = File.Create(dumpFileName);
                }
                else
                {
                    xmlInputStream = File.OpenRead(dumpFileName);
                }

                XDocument document = XDocument.Load(xmlInputStream);

                var library = document.Descendants("library").First();
                var mediaEntries = library.Descendants("MediaEntry");
                foreach (var element in mediaEntries)
                {
                    Media parsedMedia = parseMedia(element);
                    mediaLookup.Add(parsedMedia.Id, parsedMedia);
                }
            } catch (Exception e)
            {
                //
            } finally
            {
                xmlInputStream?.Close();
            }
            
        }


        private static XElement serializeMedia(Media media)
        {
            XElement doc = new XElement("MediaEntry");
            using (var writer = doc.CreateWriter())
            {
                mediaSerde.WriteObject(writer, media);
            }
            return doc;
        }

        private static Media parseMedia(XElement mediaEntry)
        {
            var xml = mediaEntry.FirstNode;
            using (var reader = xml.CreateReader())
            {
                return mediaSerde.ReadObject(reader) as Media;
            }
        }


        public void PersistCollection()
        {
            XElement library = new XElement("library",
                mediaLookup.Values.Select(serializeMedia).ToArray()
            );
            XDocument root = new XDocument(library);
            root.Save(dumpFileName);
        }
        

        public void AddMedia(Media media)
        {
            mediaLookup.Add(media.Id, media);
        }

        public Media GetById(Guid id)
        {
            return mediaLookup[id];
        }

        public ICollection<Media> FetchAll()
        {
            return mediaLookup.Values;
        }

        public void RemoveById(Guid id)
        {
            mediaLookup.Remove(id);
        }

        public void UpdateById(Guid id, Media newMedia)
        {
            if (mediaLookup.ContainsKey(id))
            {
                mediaLookup[id] = newMedia;
                mediaLookup[id].Id = id;
            }
            else mediaLookup.Add(id, newMedia);
        }
    }
}
