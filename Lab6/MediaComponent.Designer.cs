﻿namespace Lab6
{
    partial class MediaComponent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layout = new System.Windows.Forms.TableLayoutPanel();
            this.artist = new System.Windows.Forms.TextBox();
            this.title = new System.Windows.Forms.TextBox();
            this.duration = new System.Windows.Forms.TextBox();
            this.expandBtn = new System.Windows.Forms.Button();
            this.additionalInfo = new Lab6.AdditionalMediaInfoComponent();
            this.layout.SuspendLayout();
            this.SuspendLayout();
            // 
            // layout
            // 
            this.layout.ColumnCount = 2;
            this.layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.02817F));
            this.layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.97183F));
            this.layout.Controls.Add(this.title, 0, 0);
            this.layout.Controls.Add(this.artist, 0, 1);
            this.layout.Controls.Add(this.duration, 1, 0);
            this.layout.Controls.Add(this.expandBtn, 1, 1);
            this.layout.Controls.Add(this.additionalInfo, 0, 2);
            this.layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout.Location = new System.Drawing.Point(0, 0);
            this.layout.Name = "layout";
            this.layout.RowCount = 3;
            this.layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.layout.Size = new System.Drawing.Size(355, 113);
            this.layout.TabIndex = 0;
            // 
            // artist
            // 
            this.artist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.artist.Location = new System.Drawing.Point(3, 58);
            this.artist.Name = "artist";
            this.artist.ReadOnly = true;
            this.artist.Size = new System.Drawing.Size(270, 22);
            this.artist.TabIndex = 0;
            this.artist.WordWrap = false;
            // 
            // title
            // 
            this.title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.title.Location = new System.Drawing.Point(3, 12);
            this.title.Name = "title";
            this.title.ReadOnly = true;
            this.title.Size = new System.Drawing.Size(270, 22);
            this.title.TabIndex = 1;
            this.title.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.title.WordWrap = false;
            // 
            // duration
            // 
            this.duration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.duration.Location = new System.Drawing.Point(279, 12);
            this.duration.Name = "duration";
            this.duration.ReadOnly = true;
            this.duration.Size = new System.Drawing.Size(73, 22);
            this.duration.TabIndex = 2;
            this.duration.WordWrap = false;
            // 
            // expandBtn
            // 
            this.expandBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.expandBtn.Location = new System.Drawing.Point(279, 57);
            this.expandBtn.Name = "expandBtn";
            this.expandBtn.Size = new System.Drawing.Size(73, 23);
            this.expandBtn.TabIndex = 3;
            this.expandBtn.Text = "More...";
            this.expandBtn.UseVisualStyleBackColor = true;
            // 
            // additionalInfo
            // 
            this.layout.SetColumnSpan(this.additionalInfo, 2);
            this.additionalInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.additionalInfo.Location = new System.Drawing.Point(3, 95);
            this.additionalInfo.Name = "additionalInfo";
            this.additionalInfo.Size = new System.Drawing.Size(349, 15);
            this.additionalInfo.TabIndex = 4;
            // 
            // MediaComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layout);
            this.Name = "MediaComponent";
            this.Size = new System.Drawing.Size(355, 113);
            this.layout.ResumeLayout(false);
            this.layout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel layout;
        private System.Windows.Forms.TextBox title;
        private System.Windows.Forms.TextBox artist;
        private System.Windows.Forms.TextBox duration;
        private System.Windows.Forms.Button expandBtn;
        private AdditionalMediaInfoComponent additionalInfo;
    }
}
