﻿using System;

namespace Lab6
{
    [Serializable]
    public class Media
    {
        public Guid Id;
        public readonly string Title;
        public readonly string Artist;
        public readonly string Genre;
        public readonly string Album;
        public readonly int DurationSeconds;

        public Media(Guid id, string title, string artist, string genre, string album, int durationS)
        {
            this.Id = id;
            this.Title = title;
            this.Artist = artist;
            this.Genre = genre;
            this.Album = album;
            this.DurationSeconds = durationS;
        }

        public Media(string title, string artist, string genre, string album, int durationS) : this(
            Guid.NewGuid(),
            title,
            artist,
            genre,
            album,
            durationS
        ) { }

        public string DurationAsString()
        {
            int seconds;
            int minutes = Math.DivRem(DurationSeconds, 60, out seconds);
            return String.Format("{0:D2}:{1:D2}", minutes, seconds);
        }
    }
}
