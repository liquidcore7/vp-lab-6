﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Lab6
{
    public partial class MainWindow : Form
    {

        private readonly AudioLibraryRepo audioRepo;


        private void deleteMedia()
        {
            MediaComponent maybeSelectedItem = listLayout.GetSelected() as MediaComponent;

            if (maybeSelectedItem != null)
            {
                var selectedMediaId = maybeSelectedItem.GetMedia().Id;
                audioRepo.RemoveById(selectedMediaId);
                drawCollection();
            }
        }


        private void addOrEditMedia()
        {
            AddMediaForm addMediaForm = new AddMediaForm();
            MediaComponent maybeSelectedItem = listLayout.GetSelected() as MediaComponent;

            if (maybeSelectedItem != null) // edit mode
            {
                addMediaForm.Text = "Edit media...";
                Media selectedMedia = maybeSelectedItem.GetMedia();
                addMediaForm.SetMedia(selectedMedia);

                addMediaForm.RegisterHandler((object sender, EventArgs args) =>
                {
                    Media savedMedia = addMediaForm.GetMedia();
                    audioRepo.UpdateById(selectedMedia.Id, savedMedia);
                    addMediaForm.Close();
                    drawCollection();
                });

            } else // add new
            {
                addMediaForm.Text = "Add media...";
                addMediaForm.RegisterHandler((object sender, EventArgs args) =>
                {
                    Media savedMedia = addMediaForm.GetMedia();
                    audioRepo.AddMedia(savedMedia);
                    addMediaForm.Close();
                    drawCollection();
                });
            }

            addMediaForm.ShowDialog();
        }


        private void drawCollection()
        {
            var mediaComponents = audioRepo
                .FetchAll()
                .Select(MediaComponent.FromMedia)
                .ToArray();

            listLayout.Clear();

            if (mediaComponents.Count() == 0)
            {
                var textbox = new TextBox();
                textbox.Text = "No songs added";
                textbox.TextAlign = HorizontalAlignment.Center;
                textbox.Dock = DockStyle.Fill;

                listLayout.AddControl(textbox);
            } else
            {
                listLayout.AddAll(mediaComponents);
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            audioRepo = new AudioLibraryRepo();

            FormClosed += (object sender, FormClosedEventArgs args) =>
            {
                audioRepo.PersistCollection();
            };

            Load += (object sender, EventArgs args) =>
            {
                drawCollection();
            };

            saveMediaBtn.Click += (object sender, EventArgs args) =>
            {
                addOrEditMedia();
                listLayout.ClearSelection();
            };

            deleteBtn.Click += (object sender, EventArgs args) =>
            {
                deleteMedia();
                listLayout.ClearSelection();
            };

            listLayout.RegisterOnSelected(selectedControl => {
                saveMediaBtn.Text = "Edit media...";
                deleteBtn.Enabled = true;
            });

            listLayout.RegisterOnUnSelected(unselected =>
            {
                saveMediaBtn.Text = "Add media...";
                deleteBtn.Enabled = false;
            });

        }



        





    }
}
