﻿using System.Windows.Forms;
using System.Drawing;
using System;

namespace Lab6
{
    public partial class VerticalLinearLayout : UserControl
    {
        private int verticalMargin = 10;
        private int horizontalMargin = 10;
        private Control selected = null;
        private Color? savedBackColor = null;
        private Action<Control> onSelected;
        private Action<Control> onUnSelected;

        private void redraw()
        {
            var layoutPos = Bounds.Location;
            var width = Width - horizontalMargin * 2;
            var leftX = layoutPos.X + horizontalMargin;

            int totalHeight = verticalMargin;
            for (int i = 0; i < panel.Controls.Count; ++i)
            {
                var topY = totalHeight;
                var controlHeight = panel.Controls[i].Height;
                panel.Controls[i].SetBounds(leftX, topY, width, controlHeight);

                totalHeight += controlHeight + verticalMargin;
            }
        }

        private Control getByPos(Point point)
        {
            foreach (Control c in panel.Controls)
            {
                if (c.Bounds.Contains(point))
                {
                    return c;
                }
            }
            return null;
        }

        private void mouseClickCallback(object sender, MouseEventArgs args)
        {
            if (args.Button == MouseButtons.Right)
            {
                Control sndr = sender as Control;
                var screenLoc = sndr.PointToScreen(args.Location);

                var newSelected = getByPos(PointToClient(screenLoc));

                ClearSelection();

                if (newSelected != null)
                {
                    selected = newSelected;
                    savedBackColor = selected.BackColor;
                    selected.BackColor = Color.LightBlue;
                    onSelected?.Invoke(selected);
                }
            } 
        }

        private void registerMouseDown(ControlCollection controls, Action<Control, MouseEventHandler> action)
        {
            foreach (Control c in controls)
            {
                action(c, mouseClickCallback);
                registerMouseDown(c.Controls, action);
            }
        }
        private void registerMouseDown(Control control, Action<Control, MouseEventHandler> action)
        {
            action(control, mouseClickCallback);
            registerMouseDown(control.Controls, action);
        }

        public VerticalLinearLayout()
        {
            InitializeComponent();

            Paint += (object sender, PaintEventArgs args) =>
            {
                redraw();
            };

            MouseClick += mouseClickCallback;
        }


        public void AddControl(Control control)
        {
            registerMouseDown(control, (ctrl, handler) => ctrl.MouseDown += handler);
            panel.Controls.Add(control);
            Invalidate();
        }

        public void AddAll(Control[] controls)
        {
            foreach (Control c in controls)
            {
                registerMouseDown(c, (ctrl, handler) => ctrl.MouseDown += handler);
            }
            panel.Controls.AddRange(controls);
            Invalidate();
        }

        public void RemoveControl(Control ctrl)
        {
            registerMouseDown(ctrl, (c, handler) => c.MouseDown -= handler);
            panel.Controls.Remove(ctrl);
            Invalidate();
        }

        public Control GetSelected()
        {
            return selected;
        }

        public void ClearSelection()
        {
            if (selected != null)
            {
                selected.BackColor = savedBackColor.Value;
                savedBackColor = null;
                selected = null;
                onUnSelected?.Invoke(selected);
            } 
        }

        public void Clear()
        {
            foreach (Control c in panel.Controls)
            {
                registerMouseDown(c, (ctrl, handler) => ctrl.MouseDown -= handler);
            }
            panel.Controls.Clear();
            Invalidate();
        }


        public void RegisterOnSelected(Action<Control> action)
        {
            onSelected = action;
        }

        public void RegisterOnUnSelected(Action<Control> action)
        {
            onUnSelected = action;
        }

    }
}
