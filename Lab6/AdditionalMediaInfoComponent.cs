﻿using System.Windows.Forms;

namespace Lab6
{
    public partial class AdditionalMediaInfoComponent : UserControl
    {
        public AdditionalMediaInfoComponent()
        {
            InitializeComponent();
        }

        public void SetMediaInfo(Media media)
        {
            album.Text = media.Album;
            genre.Text = media.Genre;
        }

        public string GetAlbum()
        {
            return album.Text;
        }

        public string GetGenre()
        {
            return genre.Text;
        }
    }
}
