﻿namespace Lab6
{
    partial class AdditionalMediaInfoComponent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layout = new System.Windows.Forms.TableLayoutPanel();
            this.albumLabel = new System.Windows.Forms.Label();
            this.genreLabel = new System.Windows.Forms.Label();
            this.album = new System.Windows.Forms.TextBox();
            this.genre = new System.Windows.Forms.TextBox();
            this.layout.SuspendLayout();
            this.SuspendLayout();
            // 
            // layout
            // 
            this.layout.ColumnCount = 2;
            this.layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.77345F));
            this.layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.22655F));
            this.layout.Controls.Add(this.genre, 1, 1);
            this.layout.Controls.Add(this.albumLabel, 0, 0);
            this.layout.Controls.Add(this.genreLabel, 0, 1);
            this.layout.Controls.Add(this.album, 1, 0);
            this.layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout.Location = new System.Drawing.Point(0, 0);
            this.layout.Name = "layout";
            this.layout.RowCount = 2;
            this.layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout.Size = new System.Drawing.Size(437, 150);
            this.layout.TabIndex = 0;
            // 
            // albumLabel
            // 
            this.albumLabel.AutoSize = true;
            this.albumLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.albumLabel.Location = new System.Drawing.Point(3, 0);
            this.albumLabel.Name = "albumLabel";
            this.albumLabel.Size = new System.Drawing.Size(110, 75);
            this.albumLabel.TabIndex = 0;
            this.albumLabel.Text = "Album:";
            this.albumLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // genreLabel
            // 
            this.genreLabel.AutoSize = true;
            this.genreLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.genreLabel.Location = new System.Drawing.Point(3, 75);
            this.genreLabel.Name = "genreLabel";
            this.genreLabel.Size = new System.Drawing.Size(110, 75);
            this.genreLabel.TabIndex = 1;
            this.genreLabel.Text = "Genre:";
            this.genreLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // album
            // 
            this.album.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.album.Location = new System.Drawing.Point(119, 26);
            this.album.Name = "album";
            this.album.ReadOnly = true;
            this.album.Size = new System.Drawing.Size(315, 22);
            this.album.TabIndex = 2;
            this.album.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.album.WordWrap = false;
            // 
            // genre
            // 
            this.genre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.genre.Location = new System.Drawing.Point(119, 101);
            this.genre.Name = "genre";
            this.genre.ReadOnly = true;
            this.genre.Size = new System.Drawing.Size(315, 22);
            this.genre.TabIndex = 3;
            this.genre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.genre.WordWrap = false;
            // 
            // AdditionalMediaInfoComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layout);
            this.Name = "AdditionalMediaInfoComponent";
            this.Size = new System.Drawing.Size(437, 150);
            this.layout.ResumeLayout(false);
            this.layout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel layout;
        private System.Windows.Forms.Label albumLabel;
        private System.Windows.Forms.Label genreLabel;
        private System.Windows.Forms.TextBox genre;
        private System.Windows.Forms.TextBox album;
    }
}
