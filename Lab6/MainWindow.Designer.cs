﻿namespace Lab6
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.saveMediaBtn = new System.Windows.Forms.Button();
            this.listLayout = new Lab6.VerticalLinearLayout();
            this.deleteBtn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.125F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.875F));
            this.tableLayoutPanel1.Controls.Add(this.saveMediaBtn, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.listLayout, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.deleteBtn, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 301F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 450);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // saveMediaBtn
            // 
            this.saveMediaBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saveMediaBtn.Location = new System.Drawing.Point(636, 3);
            this.saveMediaBtn.Name = "saveMediaBtn";
            this.saveMediaBtn.Size = new System.Drawing.Size(161, 66);
            this.saveMediaBtn.TabIndex = 1;
            this.saveMediaBtn.Text = "Add media";
            this.saveMediaBtn.UseVisualStyleBackColor = true;
            // 
            // listLayout
            // 
            this.listLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listLayout.Location = new System.Drawing.Point(3, 3);
            this.listLayout.Name = "listLayout";
            this.tableLayoutPanel1.SetRowSpan(this.listLayout, 3);
            this.listLayout.Size = new System.Drawing.Size(627, 444);
            this.listLayout.TabIndex = 2;
            // 
            // deleteBtn
            // 
            this.deleteBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deleteBtn.Enabled = false;
            this.deleteBtn.Location = new System.Drawing.Point(636, 75);
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(161, 71);
            this.deleteBtn.TabIndex = 3;
            this.deleteBtn.Text = "Delete media";
            this.deleteBtn.UseVisualStyleBackColor = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainWindow";
            this.Text = "Lab 6";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button saveMediaBtn;
        private VerticalLinearLayout listLayout;
        private System.Windows.Forms.Button deleteBtn;
    }
}

